#!/bin/bash

THIS_DIR=$PWD
SAT4J_DIR=sat4j-2.3.6-SNAPSHOT

(test -d "$SAT4J_DIR") && rm -rf "$SAT4J_DIR"
mkdir "$SAT4J_DIR"
cd "$SAT4J_DIR"
git init
git remote add origin https://gitlab.ow2.org/sat4j/sat4j.git
git fetch origin 8dadb81569350aa5771c71119eaf12833536745e
git reset --hard FETCH_HEAD
cd 'org.sat4j.core'
mvn -Dmaven.javadoc.skip=true install
cd ..
mvn -Dmaven.test.skip=true -Dmaven.javadoc.skip=true install
cd "$THIS_DIR"

mvn install:install-file -Dfile=./lib/xcsp3parser.jar -DgroupId=org.xcsp3 -DartifactId=org.xcsp3.parser -Dversion=3.0 -Dpackaging=jar
mvn -f org.sat4j.csp/pom.xml clean install
(test -e sat4j-csp.jar) || ln -s org.sat4j.csp/target/org.ow2.sat4j.csp-2.3.6-SNAPSHOT-jar-with-dependencies.jar sat4j-csp.jar
