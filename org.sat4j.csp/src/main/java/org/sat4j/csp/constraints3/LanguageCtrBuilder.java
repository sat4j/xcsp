/*******************************************************************************
 * SAT4J: a SATisfiability library for Java Copyright (C) 2004-2016 Daniel Le Berre
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU Lesser General Public License Version 2.1 or later (the
 * "LGPL"), in which case the provisions of the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of the LGPL, and not to allow others to use your version of
 * this file under the terms of the EPL, indicate your decision by deleting
 * the provisions above and replace them with the notice and other provisions
 * required by the LGPL. If you do not delete the provisions above, a recipient
 * may use your version of this file under the terms of the EPL or the LGPL.
 *******************************************************************************/
package org.sat4j.csp.constraints3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.sat4j.csp.intension.ICspToSatEncoder;
import org.sat4j.reader.XMLCSP3Reader;
import org.xcsp.parser.entries.XVariables.XVarInteger;

/**
 * A constraint builder for XCSP3 instance format.
 * Used by {@link XMLCSP3Reader}.
 * This class is dedicated to language (regular, MDD) constraints.
 * 
 * @author Emmanuel Lonca - lonca@cril.fr
 *
 */
public class LanguageCtrBuilder {

	/** the solver in which the problem is encoded */
	private ICspToSatEncoder solver;

	public LanguageCtrBuilder(ICspToSatEncoder solver) {
		this.solver = solver;
	}

	public boolean buildCtrRegular(final String id, final XVarInteger[] list, final Object[][] objTransitions, final String startState, final String[] finalStates) {
		final Map<String, List<RegularTransition>> transitions = buildTransitionMap(objTransitions);
		return buildCtrRegular(list, transitions, startState, finalStates);
	}
	
	private boolean buildCtrRegular(final XVarInteger[] list, final Map<String, List<RegularTransition>> transitions, final String startState, final String[] finalStates) {
		final Set<String> states = transitions.values().stream().flatMap(List::stream).map(t -> new String[] {t.getFromState(), t.getToState()}).flatMap(Arrays::stream).collect(Collectors.toSet());
		final Map<String, Map<Integer, Integer>> satVars = new HashMap<>();
		states.forEach(s -> satVars.put(s, IntStream.range(0, list.length+1).mapToObj(i -> i).collect(Collectors.toMap(i -> i, i -> this.solver.newSatSolverVar()))));
		this.solver.addClause(new int[]{satVars.get(startState).get(0)});
		states.stream().filter(s -> !s.equals(startState)).forEach(s -> this.solver.addClause(new int[]{-satVars.get(s).get(0)}));
		for(final String state : states) {
			final List<RegularTransition> transitionsTo = transitions.get(state);
			final Map<Integer, Integer> stateVars = satVars.get(state);
			if(transitionsTo == null || transitionsTo.isEmpty()) {
				IntStream.range(1, list.length+1).map(stateVars::get).forEach(v -> this.solver.addClause(new int[]{-v}));
			}
			else {
				for(int i=1; i<list.length+1; ++i) {
					final List<List<Integer>> prequisites = computePrequisites(transitionsTo, i, list[i-1].id, satVars);
					encodeTransition(prequisites, stateVars.get(i));
				}
			}
		}
		this.solver.addClause(Arrays.stream(finalStates).mapToInt(s -> satVars.get(s).get(list.length)).toArray());
		return false;
	}

	private List<List<Integer>> computePrequisites(final List<RegularTransition> transitionsTo, final int time, final String cspVar, final Map<String, Map<Integer, Integer>> reachableStatesVars) {
		final List<List<Integer>> prerequisites = new ArrayList<>();
		for(final RegularTransition transition : transitionsTo) {
			final String from = transition.getFromState();
			final List<Integer> prerequisite = new ArrayList<>();
			prerequisite.add(reachableStatesVars.get(from).get(time-1));
			prerequisite.add(this.solver.getSolverVar(cspVar, (int) transition.getValue()));
			prerequisites.add(prerequisite);
		}
		return prerequisites;
	}
	
	private void encodeTransition(final List<List<Integer>> prequisites, final int to) {
		final List<Integer> prereqVars = new ArrayList<>();
		for(final List<Integer> prereq : prequisites) {
			final Integer var = this.solver.newSatSolverVar();
			prereq.forEach(l -> this.solver.addClause(new int[]{-var, l}));
			this.solver.addClause(IntStream.concat(prereq.stream().mapToInt(i -> -i), IntStream.of(var)).toArray());
			prereqVars.add(var);
		}
		prereqVars.stream().forEach(v -> this.solver.addClause(new int[]{-v, to}));
		this.solver.addClause(IntStream.concat(prereqVars.stream().mapToInt(i -> i), IntStream.of(-to)).toArray());
	}


	private Map<String, List<RegularTransition>> buildTransitionMap(Object[][] objTransitions) {
		final Map<String, List<RegularTransition>> transitions = new HashMap<>(objTransitions.length);
		for(final Object[] transition : objTransitions) {
			final String toState = (String) transition[2];
			List<RegularTransition> toStateTransitions = transitions.computeIfAbsent(toState, k -> new ArrayList<>());
			toStateTransitions.add(new RegularTransition((String) transition[0], (long) transition[1], toState));
		}
		return transitions;
	}


	public class RegularTransition {
		private String fromState;
		private long value;
		private String toState;

		private RegularTransition(final String fromState, final long value, final String toState) {
			this.fromState = fromState;
			this.value = value;
			this.toState = toState;
		}

		public String getFromState() {
			return fromState;
		}

		public long getValue() {
			return value;
		}

		public String getToState() {
			return toState;
		}

		@Override
		public String toString() {
			return fromState+" --["+value+"]--> "+toState;
		}
	}

	public boolean buildCtrMDD(String id, XVarInteger[] list, Object[][] objTransitions) {
		final MddToRegularHelper helper = new MddToRegularHelper(list, objTransitions);
		return helper.buildCtr();
	}

	private class MddToRegularHelper {
		
		private XVarInteger[] list;
		
		private Map<String, List<RegularTransition>> transitions;
		
		private String startState;
		
		private String finalState;

		private MddToRegularHelper(final XVarInteger[] list, final Object[][] objTransitions)  {
			this.list = list;
			this.transitions = buildTransitionMap(objTransitions);
		}
		
		private boolean buildCtr() {
			computeRootAndFinalNodes();
			return buildCtrRegular(list, this.transitions, this.startState, new String[]{this.finalState});
		}

		private void computeRootAndFinalNodes() {
			final Set<String> states = new HashSet<>();
			this.transitions.values().stream().forEach(l -> l.stream().map(RegularTransition::getFromState).forEach(states::add));
			this.transitions.values().stream().forEach(l -> l.stream().map(RegularTransition::getToState).forEach(states::add));
			final Map<String, Integer> level = new HashMap<>();
			states.stream().filter(state -> !this.transitions.containsKey(state)).collect(Collectors.toList()).stream().forEach(state -> level.put(state, 0));
			if(level.size() != 1) {
				throw new IllegalArgumentException("not exactly one root node in MDD");
			}
			this.startState = level.keySet().iterator().next();
			states.removeAll(level.keySet());
			int oldSize = states.size();
			int i;
			for(i = 1; !states.isEmpty(); ++i){
				final Integer currentState = i;
				List<String> newLevel = states.stream().filter(state -> transitions.get(state).stream().anyMatch(tr -> Integer.valueOf(currentState-1).equals(level.get(tr.getFromState())))).collect(Collectors.toList());
				states.removeAll(newLevel);
				newLevel.stream().forEach(state -> level.put(state, currentState));
				if(states.size() == oldSize) {
					throw new IllegalArgumentException("loop in MDD");
				}
				oldSize = states.size();
			}
			final Integer maxLevel = i-1;
			List<String> finalStates = level.keySet().stream().filter(state -> level.get(state).equals(maxLevel)).collect(Collectors.toList());
			if(finalStates.size() > 1) {
				throw new IllegalArgumentException("more than one final node in MDD");
			}
			this.finalState = finalStates.iterator().next();
		}
	}

}
